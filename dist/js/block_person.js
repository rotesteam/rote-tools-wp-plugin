const { registerBlockType } = wp.blocks;
const { TextControl, TextareaControl, SelectControl, Button } = wp.components;
const { useSelect, useDispatch } = wp.data;

registerBlockType('rtwp/personinfo', {
    title: 'Personeninformationen',
    category: 'rtwp',
    attributes: {
        firstName: {
            type: 'string',
            source: 'meta',
            meta: '_rtwp_first_name',
        },
        lastName: {
            type: 'string',
            source: 'meta',
            meta: '_rtwp_last_name',
        },
        nickname: {
            type: 'string',
            source: 'meta',
            meta: '_rtwp_nickname',
        },
        functions: {
            type: 'array',
            source: 'meta',
            meta: '_rtwp_functions',
            default: [],
        },
    },
    edit({ attributes, setAttributes }) {
        const { firstName, lastName, nickname, functions } = attributes;
        const { editPost } = useDispatch('core/editor');
        const sections = useSelect((select) =>
            select('core').getEntityRecords('taxonomy', 'tax_rtwp_section')
        );

        function updateMetaValue(metaKey, value) {
            editPost({ meta: { [metaKey]: value } });
        }

        function updateFunction(index, key, value) {
            const updatedFunctions = [...functions];
            updatedFunctions[index] = { ...updatedFunctions[index], [key]: value };
            setAttributes({ functions: updatedFunctions });
        }

        function addFunction() {
            setAttributes({ functions: [...functions, {}] });
        }

        function removeFunction(index) {
            const updatedFunctions = [...functions];
            updatedFunctions.splice(index, 1);
            setAttributes({ functions: updatedFunctions });
        }

        return (
            <>
                <TextControl
                    label="Vorname"
                    value={firstName}
                    onChange={(value) => updateMetaValue('_rtwp_first_name', value)}
                />
                <TextControl
                    label="Nachname"
                    value={lastName}
                    onChange={(value) => updateMetaValue('_rtwp_last_name', value)}
                />
                <TextControl
                    label="Spitzname"
                    value={nickname}
                    onChange={(value) => updateMetaValue('_rtwp_nickname', value)}
                />
                <h4>Funktionen</h4>
                {functions.map((func, index) => (
                    <div key={index}>
                        <TextControl
                            label="Funktionsbezeichnung"
                            value={func.title}
                            onChange={(value) => updateFunction(index, 'title', value)}
                        />
                        <SelectControl
                            label="Gremium"
                            value={func.section}
                            options={[
                                { label: '-- auswählen --', value: '' },
                                ...(sections || []).map((section) => ({
                                    label: section.name,
                                    value: section.id,
                                })),
                            ]}
                            onChange={(value) => updateFunction(index, 'section', value)}
                        />
                        <TextareaControl
                            label="Beschreibung"
                            value={func.description}
                            onChange={(value) => updateFunction(index, 'description', value)}
                        />
                        <Button isLink onClick={() => removeFunction(index)}>
                            Funktion entfernen
                        </Button>
                    </div>
                ))}
                <Button isSecondary onClick={addFunction}>
                    Funktion hinzufügen
                </Button>
            </>
        );
    },
    save() {
        return null;
    },
});
