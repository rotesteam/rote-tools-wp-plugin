const { registerPlugin } = wp.plugins;
const { PluginDocumentSettingPanel } = wp.editPost;
const { Button } = wp.components;
const { useDispatch } = wp.data;

function MySaveButton() {
    const { editPost, savePost } = useDispatch( 'core/editor' );
    function saveWithDirtyStatus() {
        editPost( { content: ' ' } );
        savePost();
    }
    return (
        <PluginDocumentSettingPanel
            name="my-save-button"
            title="My Save Button"
            className="my-save-button"
        >
            <Button
                isPrimary
                onClick={saveWithDirtyStatus}
            >
                Save with Dirty Status
            </Button>
        </PluginDocumentSettingPanel>
    );
}
registerPlugin( 'my-save-button', { render: MySaveButton } );