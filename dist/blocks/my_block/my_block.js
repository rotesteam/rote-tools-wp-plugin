const { registerBlockType } = wp.blocks;
const { InspectorControls } = wp.blockEditor;
const { PanelBody, SelectControl } = wp.components;
const { useSelect } = wp.data;
const { withSelect } = wp.data;

registerBlockType('my-plugin/committee-info', {
    title: 'Gremiumsinformationen',
    category: 'common',
    attributes: {
        committee: {
            type: 'string',
        },
    },
    edit({ attributes, setAttributes }) {
        const { committee } = attributes;
        const committees = useSelect((select) =>
            select('core').getEntityRecords('taxonomy', 'tax_rtwp_section')
        );

        return wp.element.createElement(
            wp.Fragment,
            {},
            wp.element.createElement(
                InspectorControls,
                {},
                wp.element.createElement(
                    PanelBody,
                    { title: 'Einstellungen' },
                    wp.element.createElement(
                        SelectControl,
                        {
                            label: 'Gremium',
                            value: committee,
                            options: [
                                { label: '-- auswählen --', value: '' },
                                ...(committees || []).map((committee) => ({
                                    label: committee.name,
                                    value: committee.id,
                                })),
                            ],
                            onChange: (value) => setAttributes({ committee: value }),
                        }
                    )
                )
            ),
            wp.element.createElement(CommitteeInfoFrontend, { committee })
        );
    },
    save() {
        return null;
    },
});

const CommitteeInfoFrontend = withSelect((select, ownProps) => {
    const { committee } = ownProps;
    if (!committee) {
        return {};
    }
    const committeeData = select('core').getEntityRecord(
        'taxonomy',
        'tax_rtwp_section',
        committee
    );
    const persons = select('core').getEntityRecords('postType', 'rtwp-person', {
        tax_rtwp_section: committee,
    });
    return { committee: committeeData, persons };
})(({ committee, persons }) => {
    if (!committee || !persons) {
        return null;
    }

    return wp.element.createElement(
        'div',
        {},
        wp.element.createElement('h3', {}, committee.name),
        wp.element.createElement(
            'ul',
            {},
            persons.map((person) =>
                wp.element.createElement(
                    'li',
                    { key: person.id },
                    person.title.rendered +
                        ' - ' +
                        person.meta._rtwp_functions.map((func) => func.title).join(', ')
                )
            )
        )
    );
});
