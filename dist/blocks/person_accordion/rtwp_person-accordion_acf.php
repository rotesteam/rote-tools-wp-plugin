<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;


if( function_exists('acf_add_local_field_group') ):

	$personaccordion_field_person[]=array(//rtwp_personaccodion_person
		'key' => 'field_rtwp_personaccordion_person',
		'label' => 'Person(en)',
		'name' => 'rtwp_personaccordion_person',
		'type' => 'select',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
        'return_format' => 'id',
    );

$personaccordion_block_fields=array_merge(
    $personaccordion_field_person,
);

acf_add_local_field_group(array(
	'key' => 'group_rtwp_personaccordion_block',
	'title' => '',
	'fields' => $personaccordion_block_fields,
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'rtwp/person-accordion',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;