const { registerBlockType } = wp.blocks;
const { SelectControl } = wp.components;
const { useSelect } = wp.data;
const { withSelect } = wp.data;

registerBlockType('rtwp/person', {
    title: 'Gremiumsinformationen',
    category: 'common',
    attributes: {
        committee: {
            type: 'string',
        },
    },
    edit({ attributes, setAttributes }) {
        const { committee } = attributes;
        const committees = useSelect((select) =>
            select('core').getEntityRecords('taxonomy', 'tax_rtwp_section')
        );

        return (
            <>
                <SelectControl
                    label="Gremium"
                    value={committee}
                    options={[
                        { label: '-- auswählen --', value: '' },
                        ...(committees || []).map((committee) => ({
                            label: committee.name,
                            value: committee.id,
                        })),
                    ]}
                    onChange={(value) => setAttributes({ committee: value })}
                />
            </>
        );
    },
    save() {
        return null;
    },
});

const CommitteeInfoFrontend = ({ committee, persons }) => {
    if (!committee || !persons) {
        return null;
    }

    return (
        <div>
            <h3>{committee.name}</h3>
            <ul>
                {persons.map((person) => (
                    <li key={person.id}>
                        {person.title.rendered} -{' '}
                        {person.meta._rtwp_functions.map((func) => func.title).join(', ')}
                    </li>
                ))}
            </ul>
        </div>
    );
};

wp.hooks.addFilter(
    'blocks.getSaveContent.extraProps',
    'my-plugin/committee-info',
    (props, blockType, attributes) => {
        if (blockType.name !== 'my-plugin/committee-info') {
            return props;
        }

        return {
            ...props,
            children: wp.element.createElement(
                withSelect((select) => {
                    const { committee } = attributes;
                    if (!committee) {
                        return {};
                    }
                    const committeeData = select('core').getEntityRecord(
                        'taxonomy',
                        'tax_rtwp_section',
                        committee
                    );
                    const persons = select('core').getEntityRecords('postType', 'rtwp-person', {
                        tax_rtwp_section: committee,
                    });
                    return { committee: committeeData, persons };
                })(CommitteeInfoFrontend),
                attributes
            ),
        };
    }
);
