<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;


if( function_exists('acf_add_local_field_group') ):

	$themetile_fields_title[]=array(//rtwp_themetile_title
		'key' => 'field_rtwp_themetile_title',
		'label' => 'Titel',
		'name' => 'rtwp_themetile_title',
		'type' => 'text',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '50',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Hier kann der Titel angegeben werden',
	);

	$themetile_fields_headline[]=array(//rtwp_themetile_headline
		'key' => 'field_rtwp_themetile_headline',
		'label' => 'Headline',
		'name' => 'rtwp_themetile_headline',
		'type' => 'text',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '50',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Hier kann das Thema eingegeben werden',
	);
	$themetile_fields_urltype[]=array(//rtwp_themetile_urltype
		'key' => 'field_rtwp_themetile_urltype',
		'label' => 'Link-Typ',
		'name' => 'rtwp_themetile_urltype',
		'type' => 'true_false',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
		  'width' => '30',
		  'class' => '',
		  'id' => '',
		),
		'message' => '',
		'default_value' => 0,
		'ui' => 1,
		'ui_on_text' => 'extern',
		'ui_off_text' => 'intern',
	);
	
	$themetile_fields_url_extern[]=array(//rtwp_themetile_url_extern
		'key' => 'field_rtwp_themetile_url_extern',
		'label' => 'Externe Adresse',
		'name' => 'rtwp_themetile_url_extern',
		'type' => 'url',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' =>array(
			array(
			  array(
				'field' => 'field_rtwp_themetile_urltype',
				'operator' => '==',
				 'value' => '1',
			  ),
			),
		  ),
		'wrapper' => array(
			'width' => '70',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Hier die URL in Form von https://…',
	);
	$themetile_fields_url_intern[]=array(//rtwp_themetile_url_intern
		'key' => 'field_rtwp_themetile_url_intern',
		'label' => 'Seite oder Beitrag',
		'name' => 'rtwp_themetile_url_intern',
		'type' => 'page_link',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => array(
			array(
			  array(
				'field' => 'field_rtwp_themetile_urltype',
				'operator' => '==',
				 'value' => '0',
			  ),
			),
		  ),
		'wrapper' => array(
			'width' => '70',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Hier die Seite wählen',
	);

	$themetile_block_fields=array_merge(
		$themetile_fields_headline,
		$themetile_fields_title,
		$themetile_fields_url_extern,
		$themetile_fields_url_intern,
		$themetile_fields_urltype,
	);

acf_add_local_field_group(array(
	'key' => 'group_rtwp_themetile_block',
	'title' => '',
	'fields' => $themetile_block_fields,
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/rtwp-block-themetile',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;