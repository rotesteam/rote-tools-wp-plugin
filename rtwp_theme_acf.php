<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;


if( function_exists('acf_add_local_field_group') ):

	$navlogo_fields_large[]=array(//rtwp_navlogo_large
		'key' => 'field_rtwp_navlogo_large',
		'label' => 'Logo groß',
		'name' => 'rtwp_navlogo_large',
		'type' => 'image',
		'instructions' => 'Dieses Logo wird standardmäßig angezeigt',
		'return_format' => 'id',
		'preview_size' => 'medium',
		'library' => 'all',
		'wrapper' => array(
			'width' => '100',
		),
	);
	$navlogo_fields_small[]=array(//rtwp_navlogo_small
		'key' => 'field_rtwp_navlogo_small',
		'label' => 'Logo klein',
		'name' => 'rtwp_navlogo_small',
		'type' => 'image',
		'instructions' => 'Dieses Logo wird angezeigt, wenn die Navigation beim Scrollen nur noch schmal im oberen Bereich angezeigt wird.',
		'return_format' => 'id',
		'preview_size' => 'medium',
		'library' => 'all',
		'wrapper' => array(
			'width' => '100',
		),
	);

	$navlogo_fields=array_merge(
		$navlogo_fields_large,
		$navlogo_fields_small,
	
	);

acf_add_local_field_group(array(
	'key' => 'group_rtwp_pagelogo',
	'title' => 'Logos',
	'fields' => $navlogo_fields,
	'location' => array(
		array(
			array(
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'rtwp-adminsettings',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

$navlogo_block_fields=array_merge(
	$navlogo_fields_large,
	$navlogo_fields_small,

);

acf_add_local_field_group(array(
	'key' => 'group_rtwp_block_navlogo',
	'title' => 'Einstellungen',
	'fields' => $navlogo_block_fields,
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/rtwp-block-navlogo',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));


endif;