<?php

/**
 * Plugin Name: Rote Tools Plugin
 * Description: Stellt einige Funktionen für Wordpress bereit
 * Requires PHP: 8.0
 * Licence: AGPL
 * Version: 1.0
 * Author: Benjamin Heinrichs, Lukas Staab
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// TODO: check if acf pro is installed

include_once plugin_dir_path(__FILE__) . 'settings.php';

register_activation_hook( __FILE__, 'rt_activation_hook' );
register_deactivation_hook( __FILE__, 'rt_deactivation_hook');
register_uninstall_hook(__FILE__, 'rt_uninstall_hook');
add_action( 'admin_menu', 'rt_menu' );

function rt_activation_hook (){
    // store some default options
    // https://codex.wordpress.org/Options_API
}

function rt_deactivation_hook(){
    // clean up temporary storage
}

function rt_uninstall_hook(){
    // clean up options
}

function rt_menu(){
    $svg_data =  file_get_contents(plugin_dir_path(__FILE__) . 'dist/images/mono-rt-logo.svg');
    $svg_url = 'data:image/svg+xml;base64,' . base64_encode($svg_data);
    if( function_exists('acf_add_options_page') ) {

        $rtwp_settings_optionpage = acf_add_options_page(array(
            'page_title' 	=> 'Optionsseiten für Rote Tools Module',
            'menu_title'	=> __('Rote Tools ACF', 'rtwp'),
            'menu_slug' 	=> 'rtwp-options',
            'capability'	=> 'edit_private_pages',
            'redirect'		=> true,
            'icon_url'		=> $svg_url,
            'update_button'	=> __('Übernehmen', 'rtwp'),
            'post_id'		=> 'rtwp-options',
        ));
        // acf_add_options_sub_page(array(
        // 	'page_title' 	=> 'Einstellungen für Rote Tools',
        // 	'menu_title'	=> __('Einstellungen'),
        // 	'menu_slug'		=> 'rtwp-settings',
        // 	'capability'	=> 'edit_private_pages',
        // 	'parent_slug' 	=> $rtwp_settings_optionpage['menu_slug'],
        // 	'update_button'	=> 'Übernehmen',
        // 	'post_id'		=> 'rtwp-settings',
        // ));
        acf_add_options_sub_page(array(
            'page_title' 	=> 'Admin-Einstellungen für Rote Tools',
            'menu_title'	=> __('Administration'),
            'menu_slug'		=> 'rtwp-adminsettings',
            'capability'	=> 'edit_private_pages',
            'parent_slug' 	=> $rtwp_settings_optionpage['menu_slug'],
            'update_button'	=> 'Übernehmen',
            'post_id'		=> 'rtwp-adminsettings',
        ));

        add_menu_page(
            'Rote Tools Admin',
            'Rote Tools',
            'manage_options',
            'rote-tools',
            '',
            $svg_url
        );

        add_submenu_page(
            'rote-tools',
            'Opengraph Einstellungen', // $page_title
            'OpenGraph', // $menu_title
            'manage_options', // $capability
            'rtwp-og-settings', // $menu_slug
            'rtwp_render_option_page', // Callback
        );
    }
}


