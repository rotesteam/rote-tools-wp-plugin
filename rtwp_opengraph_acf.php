<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;


if( function_exists('acf_add_local_field_group') ):

    $opengraph_field_title[]=array(//rtwp_opengraph_title
		'key' => 'field_rtwp_opengraph_title',
		'label' => 'Titel',
		'name' => 'rtwp_opengraph_title',
		'type' => 'text',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '30',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Hier kann ein abweichender Titel angegeben werden',
    );

    $opengraph_field_desc[]=array(//rtwp_opengraph_desc
		'key' => 'field_rtwp_opengraph_desc',
		'label' => 'Beschreibung',
		'name' => 'rtwp_opengraph_desc',
		'type' => 'text',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '50',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Hier kann eine Beschreibung angegeben werden.',
    );
    $opengraph_field_sitename[]=array(//rtwp_opengraph_sitename
		'key' => 'field_rtwp_opengraph_sitename',
		'label' => 'Seitenname',
		'name' => 'rtwp_opengraph_sitename',
		'type' => 'text',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '30',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Hier kann angezeigte Seitenname geändert werden.',
    );
    $opengraph_field_url[]=array(//rtwp_opengraph_url
		'key' => 'field_rtwp_opengraph_url',
		'label' => 'Link',
		'name' => 'rtwp_opengraph_url',
		'type' => 'page_link',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '30',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => 'Abweichende Verlinkung auswählen',
    );
    $opengraph_field_image[]=array(//rtwp_opengraph_image
		'key' => 'field_rtwp_opengraph_image',
		'label' => 'Vorschaubild',
		'name' => 'rtwp_opengraph_Image',
		'type' => 'image',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '30',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
        'return' => 'id',
		'placeholder' => 'Abweichende Verlinkung auswählen',
    );

    $opengraph_fields=array_merge(
        $opengraph_field_title,
        $opengraph_field_desc,
        $opengraph_field_url,
        $opengraph_field_image,
    );
    $opengraph_fields_admin=array_merge(
        $opengraph_field_title,
        $opengraph_field_sitename,
        $opengraph_field_url,
        $opengraph_field_desc,
        $opengraph_field_image,
    );

    acf_add_local_field_group(array(
        'key' => 'group_rtwp_opengraph',
        'title' => 'OpenGraph und Suchmaschinenoptimierung',
        'fields' => $opengraph_fields,
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

    acf_add_local_field_group(array(
        'key' => 'group_rtwp_opengraph_main',
        'title' => 'OpenGraph',
        'fields' => $opengraph_fields_admin,
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'rtwp-opengraph',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
    endif;