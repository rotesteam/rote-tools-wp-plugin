<?php

/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;

include_once('rtwp_themetile_acf.php');

// Block registrieren

acf_register_block_type(array(
    'name'              => 'rtwp-block-themetile',
    'title'             => __('Themen Kachel'),
    'description'       => __('Eine Kachel die z.B. zu einem Thema oder einem Ziel verlinkt'),
    'render_callback'   => 'rtwp_block_themetile_callback',
    'category'          => 'rtwp',
    'icon' => array(
        // Specifying a background color to appear with the icon e.g.: in the inserter.
        'background' => '#fff',
        // Specifying a color for the icon (optional: if not set, a readable color will be automatically defined)
        'foreground' => '#e3000f',
        // Specifying a dashicon for the block
        'src' => 'grid-view',
    ),
    'mode'              => 'edit',
    'keywords'          => array( 'Kachel', 'Thema', 'Ziel' ),
));

// Callback

function rtwp_block_themetile_callback( $block, $content = '', $is_preview = true ) {
    
    // Create id attribute allowing for custom "anchor" value.
    $id = 'rtwp-block-themetile-' . $block['id'];
    if( !empty($block['anchor']) ) {
      $id = $block['anchor'];
    }

    // Create class attribute allowing for custom "className" and "align" values.
    $className = 'rtwp-block-themetile';
    if( !empty($block['className']) ) {
        $className .= ' ' . $block['className'];
    }
    if( !empty($block['align']) ) {
        $className .= ' align' . $block['align'];
    }



    // load the fields
    $headline = get_field( 'rtwp_themetile_headline' );
    $title = get_field( 'rtwp_themetile_title' );
    if (get_field( 'rtwp_themetile_urltype' )){
        $url = get_field( 'rtwp_themetile_url_extern' );} 
    else {
        $url = get_field( 'rtwp_themetile_url_intern' );};

    // Block

    $out = '<div id="'.$id. '" class="'. $className .' col-12 col-md-6 col-xxl-4">';
    $out .= '<div class="rtwp-tile-md">';
        $out .='<div class="tile-container">';
            $out.= '<div class="tile-element">'; 
                $out.= '<div class="element-pfeil-weiss"></div>';
            $out.= '</div>';
            $out.= '<div class="tile-content">'; 
                $out.='<div class="tile-headline">' . $headline . '</div>';
                $out.= '<div class="tile-title">' . $title . '</div>';
                $out.= '<div class="rtwp-button"><a href="' . $url . '" target="_blank"> mehr… </a></div>';
            $out.='</div>';
        $out.='</div>';
    $out .= '</div>';
    $out.='</div>';

    echo $out;
}



