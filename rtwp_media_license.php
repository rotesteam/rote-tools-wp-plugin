<?php
 
add_action( 'init', function(){
    // Register the custom taxonomy.
    register_taxonomy(
        'rtwp_tax_license',
        'attachment',
        [
            'hierarchical' => false,
            'labels' => [
                'name' => __( 'Bildrechte' ),
                'singular_name' => __( 'Bildrecht' ),
                'search_items' => __( 'Suche Bildrechte' ),
                'popular_items' => null,
                'all_items' => __( 'Alle Bildrechte' ),
                'edit_item' => __( 'Bearbeite Bildrechte' ),
                'update_item' => __( 'Aktuallisiere die Bildrechte' ),
                'add_new_item' => __( 'Füge Bildrechte hinzu' ),
                'new_item_name' => __( 'Neue Bildrechte' ),
                'separate_items_with_commas' => null,
                'add_or_remove_items' => null,
                'choose_from_most_used' => null
            ],
            'query_var' => true,
            'show_in_modal' => false,
            'rewrite' => [
                'slug' => 'licenses'
            ],
            'single' => true,
        ]
    );
   
    } 

);


add_action('rtwp_tax_license_add_form_fields','rtwp_tax_license_edit_form_fields');
add_action( 'rtwp_tax_license_edit_form_fields', 'rtwp_tax_license_edit_term_fields', 10, 2 );
add_action( 'created_rtwp_tax_license', 'rtwp_tax_license_save_term_fields' );
add_action( 'edited_rtwp_tax_license', 'rtwp_tax_license_save_term_fields' );

/* Eingabefelder für die Neuanlage eines Term */ 
function rtwp_tax_license_edit_form_fields($taxonomy){
    ?>
    <div class="form-field">
        <label for="rtwp_tax_license_url">Url zur Lizenz</label>
        <input type="text" name="rtwp_tax_license_url" id="rtwp_tax_license_url" />
        <p style="margin 0em;">Hier die Adresse zur Lizenzseite</p>
    </div>
    <div class="form-field">
        <label>Bild</label>
        <a href="#" class="button rtwp-upload">Bild hinzufügen</a>
        <a href="#" class="rtwp-remove" style="display:none">Bild entfernen</a>
        <input type="hidden" name="rtwp_tax_license_imgage" value="">
    </div>
<?php
}

/* Eingabefelder für die Term-Edit-Seite  */ 

function rtwp_tax_license_edit_term_fields( $term, $taxonomy ) {

	// get meta data value
	$text_field = get_term_meta( $term->term_id, 'rtwp_tax_license_url', true );
	$image_id = get_term_meta( $term->term_id, 'rtwp_tax_license_image', true );

	?><tr class="form-field">
		<th><label for="rtwp_tax_license_url">Link zur Lizenz</label></th>
		<td>
			<input name="rtwp_tax_license_url" id="rtwp_tax_license_url" type="text" value="<?php echo esc_url( $text_field, $protocols = 'https' ) ?>" />
			<p class="description" style="margin:0em">Link zum Lizenztext, der auch bei den jeweiligen Medien verlinkt wird.</p>
		</td>
	</tr>
	<tr class="form-field">
		<th>
			<label for="rtwp_tax_license_image">Bild</label>
		</th>
		<td>
			<?php if( $image = wp_get_attachment_image_url( $image_id, 'medium' ) ) : ?>
				<a href="#" class="rtwp-upload">
					<img src="<?php echo esc_url( $image ) ?>" />
				</a>
				<a href="#" class="rtwp-remove">Bild entfernen</a>
				<input type="hidden" name="rtwp_tax_license_image" value="<?php echo absint( $image_id ) ?>">
			<?php else : ?>
				<a href="#" class="button rtwp-upload">Bild auswählen</a>
				<a href="#" class="rtwp-remove" style="display:none">Bild entfernen</a>
				<input type="hidden" name="rtwp_tax_license_image" value="">
			<?php endif; ?>
		</td>
	</tr><?php
}


/* Speichern der Eingaben */
function rtwp_tax_license_save_term_fields( $term_id ) {
	
	update_term_meta(
		$term_id,
		'rtwp_tax_license_url',
		esc_url( $_POST[ 'rtwp_tax_license_url' ],'https' )
	);
	update_term_meta(
		$term_id,
		'rtwp_tax_license_image',
		absint( $_POST[ 'rtwp_tax_license_image' ] )
	);
	
}


// register_taxonomy_for_object_type('rtwp_tax_license','attachment');

// Quelle: https://get-up-works.de/wordpress/custom-taxonomy-metabox-media-library-modal/
function add_media_categories($fields, $post) {
    $categories = get_categories(array('taxonomy' => 'rtwp_tax_license', 'hide_empty' => 0));
    $post_categories = wp_get_object_terms($post->ID, 'rtwp_tax_license', array('fields' => 'ids'));
    $count = 0;
    foreach ($categories as $category) {
     if (in_array($category->term_id, $post_categories)) {
      $checked = ' checked="checked"';
      $count++;
     } else {
      $checked = '';  
     }
     $option = '<li><input type="checkbox" value="'.$category->category_nicename.'" id="'.$post->ID.'-'.$category->category_nicename.'" name="'.$post->ID.'-'.$category->category_nicename.'" '.$checked.'> ';
     $option .= '<label for="'.$post->ID.'-'.$category->category_nicename.'" style="padding-top:5px;">'.$category->cat_name.'</label>';
     $option .= '</li>';
     $all_cats_option .= $option;
    }
    $all_cats = '<ul id="media-categories-list" style="margin-top: 5px; margin-bottom:5px; min-height:42px; max-height:115px; overflow:auto; padding: 0 .9em; border: dashed 2px #ddd; background-color:#fdfdfd;">'; 
    if ($count > 1){
        $all_cats .= '<p><strong>Hinweis:</strong> Hier wurden mehr als eine Lizenz angeben. Es wird nur die erste berücksichtigt</p>';
    }
    $all_cats .= $all_cats_option;
    $all_cats .= '</ul> <p> Die Verlinkung auf die Lizenz erfolgt, bei notwendigkeit automatisch. </p> ';
    $all_cats .= '</fieldset>';
    $categories = array('all_categories' => array (
     'label' => __('Bildrechte'),
     'input' => 'html',
     'html' => $all_cats,
     'show_in_edit' => true,
     'show_in_modal' => true,
    ));
    return array_merge($fields, $categories);
   }
   add_filter('attachment_fields_to_edit', 'add_media_categories', null, 2);
   // save tax items assignment from media upload-/edit modal
   function add_image_attachment_fields_to_save($post, $attachment) {
    $categories = get_categories(array('taxonomy' => 'rtwp_tax_license', 'hide_empty' => 0));
    $terms = array();
    foreach($categories as $category) {
     if (isset($_POST[$post['ID'].'-'.$category->category_nicename])) {
      $terms[] = $_POST[$post['ID'].'-'.$category->category_nicename];        
     }
    }
    wp_set_object_terms( $post['ID'], $terms, 'rtwp_tax_license' );
    return $post;
   }
   add_filter('attachment_fields_to_save', 'add_image_attachment_fields_to_save', null , 2);





add_action( 'attachment_fields_to_edit', 'add_custom_attachment_fields', 10, 2 );
add_action( 'attachment_fields_to_save', 'save_custom_attachment_fields', 10, 2 );

function add_custom_attachment_fields( $form_fields, $post ) {

    // Add a new text input field
    $rtwp_title_show = get_post_meta($post->ID, 'rtwp_title_show', true);
    $rtwp_author = get_post_meta($post->ID, 'rtwp_author', true);
    $rtwp_author_url = get_post_meta($post->ID, 'rtwp_author_url', true);
    $rtwp_title_url = get_post_meta($post->ID, 'rtwp_title_url', true);
    $rtwp_license_info =get_post_meta($post->ID, 'rtwp_license_info', true);
    $form_fields['rtwp_author'] = array(
        'label' => __( 'Urheber*in', 'rtwp' ),
        'input' => 'text',
        'value' => $rtwp_author,
    );
    $form_fields['rtwp_author_url'] = array(
        'label' => __( 'Urheber*in URL', 'rtwp' ),
        'input' => 'text',
        'value' => $rtwp_author_url,
        'helps' => 'Hier kann eine URL zur Urheber*innen-Seite angeben werden.',
    );
    $form_fields['rtwp_title_show'] = array(
        'label' => __( 'Titel in Quelle angeben', 'rtwp' ),
        'input' => 'html',
        'html' => '<input type="checkbox" id="attachments-'.$post->ID.'-rtwp-title-show" name="attachments['.$post->ID.'][rtwp_title_show]" value="1"'.($rtwp_title_show ? ' checked="checked"' : '').' /> ',
        'value' => false,
    );
    $form_fields['rtwp_title_url'] = array(
        'label' => __( 'URL zum Bild/Foto', 'rtwp' ),
        'input' => 'text',
        'value' => $rtwp_title_url,
    );
    $form_fields['rtwp_license_info'] = array(
        'label' => __( 'Bearbeitungshinweis', 'rtwp' ),
        'input' => 'text',
        'value' => $rtwp_license_info,
    );
    return $form_fields;
}



function save_custom_attachment_fields( $post, $attachment ) {

// Save the author value
    if ( isset( $attachment['rtwp_author'] ) ) {
        update_post_meta( $post['ID'], 'rtwp_author', esc_attr( $attachment['rtwp_author'] ) );
    } else{
        delete_post_meta($post['ID'], 'rtwp_author' );
   }
// Save the Author URL value
    if ( isset( $attachment['rtwp_author_url'] ) ) {
    update_post_meta( $post['ID'], 'rtwp_author_url', esc_url( $attachment['rtwp_author_url'] ) );
    } else{
    delete_post_meta($post['ID'], 'rtwp_author_url' );
    }
// Save the Title show value
    if ( isset( $attachment['rtwp_title_show'] ) ) {
    update_post_meta( $post['ID'], 'rtwp_title_show', sanitize_text_field( $attachment['rtwp_title_show'] ) );
    } else{
    delete_post_meta($post['ID'], 'rtwp_title_show' );
    }
// Save the Title URL value
    if ( isset( $attachment['rtwp_title_url'] ) ) {
    update_post_meta( $post['ID'], 'rtwp_title_url', esc_url( $attachment['rtwp_title_url'] ) );
    } else{
    delete_post_meta($post['ID'], 'rtwp_title_url' );
    }
// Save the LicenseInfo value
if ( isset( $attachment['rtwp_license_info'] ) ) {
    update_post_meta( $post['ID'], 'rtwp_license_info', sanitize_text_field( $attachment['rtwp_license_info'] ) );
    } else{
    delete_post_meta($post['ID'], 'rtwp_license_info' );
    }

    return $post;  
}



// add_action( 'add_meta_boxes', 'rtwp_meta_imagelicense');

// function rtwp_meta_imagelicense() {
//     add_meta_box(
//         'image_license',
//         __( 'Image License', 'myplugin_textdomain' ),
//         'image_license_callback',
//         'post'
//     );
// };

// /**
//  * Prints the content of the metabox.
//  *
//  * @param  object $post The current post object.
//  */
// function image_license_callback( ) {
//     // Use nonce for verification
//     wp_nonce_field( plugin_basename(__FILE__), 'myplugin_noncename' );

//     // Get the saved meta value
//     $image_license = get_post_meta( $post->ID, 'image_license', true );

//     // Get all of the terms in the rtwp_tax_license taxonomy
//     $licenses = get_terms( 'rtwp_tax_license' );

//     // Print the metabox content
//     echo '<label for="image_licene">';
//     _e( 'Image Rechte', 'myplugin_textdomain' );
//     echo '</label><br>';
//     echo var_dump($licenses);
//     echo '<select name="image_license" id="image_license">';
//     foreach ( $licenses as $license ) {
//         echo '<option value="' . $license->slug . '" ' . selected( $license->slug, $image_license, false ) . '>' . $license->name . '</option>';
//     }
//     echo '</select>';
// }

// // Save metabox value
// add_action( 'save_post', function( $post_id ) {

//     // verify this came from the our screen and with proper authorization,
//     // because save_post can be triggered at other times
//     if ( !isset( $_POST['myplugin_noncename'] ) )
//         return $post_id;

//     // verify if this is an auto save routine. 
//     // If it is our form has not been submitted, so we dont want to do anything
//     if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
//         return $post_id;

//     // OK, we're authenticated: we need to find and save the data
//     $image_license = esc_attr( $_POST['image_license'] );
//     update_post_meta( $post_id, 'image_license', $image_license );

// });

// add_action( 'add_meta_boxes_attachment', function() {
//     add_meta_box(
//         'author_url',
//         __( 'Author and URL', 'myplugin_textdomain' ),
//         'author_url_callback',
//         'post'
//     );
// });

// /**
//  * Prints the content of the metabox.
//  *
//  * @param  object $post The current post object.
//  */
// function author_url_callback( $post ) {
//     // Use nonce for verification
//     wp_nonce_field( plugin_basename(__FILE__), 'myplugin_noncename' );

//     // Get the saved meta values
//     $author = get_post_meta( $post->ID, 'author', true );
//     $url = get_post_meta( $post->ID, 'url', true );

//     // Print the metabox content
//     echo '<label for="author">';
//     _e( 'Author', 'myplugin_textdomain' );
//     echo '</label><br>';
//     echo '<input type="text" name="author" id="author" value="' . esc_attr( $author ) . '" size="25" />';
//     echo '<br>';
//     echo '<label for="url">';
//     _e( 'URL', 'myplugin_textdomain' );
//     echo '</label><br>';
//     echo '<input type="text" name="url" id="url" value="' . esc_attr( $url ) . '" size="25" />';
//     echo '<br>';
//     echo '<label for="extra_field">';
//     _e( 'Extra Field', 'myplugin_textdomain' );
//     echo '</label><br>';
//     echo '<input type="text" name="extra_field" id="extra_field" value="' . esc_attr( get_post_meta( $post->ID, 'extra_field', true ) ) . '" size="25" />';
// }

// // Save metabox value
// add_action( 'save_post_attachment', function( $post_id ) {

//     // verify this came from the our screen and with proper authorization,
//     // because save_post can be triggered at other times
//     if ( !isset( $_POST['myplugin_noncename'] ) )
//         return $post_id;

//     // verify if this is an auto save routine. 
//     // If it is our form has not been submitted, so we dont want to do anything
//     if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
//         return $post_id;

//     // OK, we're authenticated: we need to find and save the data
//     $author = esc_attr( $_POST['author'] );
//     $url = esc_attr( $_POST['url'] );
//     $extra_field = esc_attr( $_POST['extra_field'] );
//     update_post_meta( $post_id, 'author', $author );
//     update_post_meta( $post_id, 'url', $url );
//     update_post_meta( $post_id, 'extra_field', $extra_field );

// });

// add_filter( 'image_send_to_editor', function( $html, $id, $caption, $title, $align, $url, $size, $alt ) {

//     // Remove the extra code added by the Imageblock.
//     $html = preg_replace( '/class="wp-image-(.*?)">/', '', $html );

//     // Add a custom class to the image.
//     $html = str_replace( '>', ' class="my-custom-class">', $html );

//     // Add a next div to the image.
//     $html .= '<div class="next-div">This is the next div</div>';

//     return $html;

// }, 10, 8 );

function rtwp_image_block_filter( $block ) {

    // Get the image ID from the block data.
    $image_id = $block['data']['attributes']['src'];
  
    // Call the rtwp_ml_over() function to get the modal overlay HTML.
    $overlay_html = rtwp_ml_over( $image_id );
  
    // Add the modal overlay HTML to the block.
    $block['html'] .= $overlay_html;
  
    return $block;
  }

  //add_filter( 'block_content_core/image', 'rtwp_image_block_filter' );

// Funktion zum Hinzufügen der Bildrechte 

function rtwp_ml_over($img_id, $img_pre=''){
    
    $attachment_array=get_post_meta($img_id);
    
    // Autor 
    $out_author = false;
    if (array_key_exists('rtwp_author', $attachment_array) AND array_key_exists('rtwp_author_url', $attachment_array)){
        $author = $attachment_array['rtwp_author'][0];
        $author_url = $attachment_array['rtwp_author_url'][0];
        $out_author = '<a href="'. $author_url .'" target="_blank" title="Link aufrufen - '.$author.'">';
        $out_author .= $author ;
        $out_author .= '</a>';
    } elseif (array_key_exists('rtwp_author', $attachment_array)){
        $author = $attachment_array['rtwp_author'][0];
        $out_author = $author;
    }

    //Title
    $out_title = false;
    if (array_key_exists('rtwp_title_show', $attachment_array)){
        $title = get_the_title($img_id);
        if($title){
            $out_title ='"';
            if (array_key_exists('rtwp_title_url', $attachment_array)){
                $title_url = get_post_meta($img_id)['rtwp_title_url'][0];
            } else {
                $title_url = false;
            }
            if ($title_url){
                $out_title .= '<a href="'.$title_url . '" target="_blank" title="Zur Quelle">';
                $out_title .=  $title . '</a>';
            }else{
                $out_title .= $title;
            }            
            $out_title .= '"';
        }
    }

    //Bildrechte
    $out_license = false;
    if (wp_get_object_terms( $img_id, 'rtwp_tax_license')){
        $terms = wp_get_object_terms( $img_id, 'rtwp_tax_license')[0];
        $term_array = get_term_meta($terms->term_id);
        if(array_key_exists('rtwp_tax_license_url', $term_array)){
            $term_url =  $term_array['rtwp_tax_license_url'][0];
        } else{
            $term_url = false;
        }
        if ($term_url){
            $out_license= '<a href="'. $term_url .'" target="_blank" title="Seite mit Lizenzbedingen aufrufen">';
            $out_license .= $terms->name; ;
            $out_license .= '</a>';
        } else{
            $out_license .= $terms->name;
        }
        if(array_key_exists('rtwp_license_info', $attachment_array)){
            $license_info = $attachment_array['rtwp_license_info'][0];
            if($license_info){
            $out_license .= ' - ' . $license_info;}
        }
        
    }  


    // Ausgabe
    $out = '<div class="rtwp-ml-over">';
    if ($img_pre){
        $out .= '<span class="rtwp-license-pretext"> '. $img_pre .'</span>';
    }
    $out .= $out_author;
    if ($out_author){
        $out .= ' | ';
    };
    $out .= $out_title;
    if ($out_author or $out_title){
        $out .= ' | ';
    };
    $out .= $out_license;   
    $out .= '</div>';

    return $out;
};

function rtwp_block_wrapper( $block_content, $block ) {
	if ( $block['blockName'] === 'core/image' ) {
        // Get the image ID from the block attributes.
        $image_id = $block['attrs']['id'];
        $license = rtwp_ml_over($image_id);
        //$content = str_replace( '<img', '<div class="rtwp-image"><div class="img-container"> <img', $block_content );
		//$content = str_replace( '</figure>', '</div>' . $license . '</div></figure>', $content );
        $content = str_replace( '</figure>',  $license . '</figure>', $block_content );

		return $content;
	} elseif ( $block['blockName'] === 'core/cover' ) {
		// Get the image ID from the block attributes.
        $image_id = $block['attrs']['id'];
        $license = rtwp_ml_over($image_id);
        //$content = str_replace( '<img', '<div class="rtwp-image"><div class="img-container"> <img', $block_content );
		//$content = str_replace( '</figure>', '</div>' . $license . '</div></figure>', $content );
        $content = str_replace( '</div></div>', '</div>' . $license . '</div>', $block_content );
		return $content;
	} elseif ( $block['blockName'] === 'core/media-text' ) {
        // Get the image ID from the block attributes.
        $image_id = $block['attrs']['mediaId'];
        //echo var_dump($block);
        $license = rtwp_ml_over($image_id);
        //$content = str_replace( '<img', '<div class="rtwp-image"><div class="img-container"> <img', $block_content );
		//$content = str_replace( '</figure>', '</div>' . $license . '</div></figure>', $content );
        $content = str_replace( '</figure>',  $license . '</figure>', $block_content );

		return $content;
    }    

	return $block_content;
}

add_filter( 'render_block', 'rtwp_block_wrapper', 10, 2 );

// function rtwp_blocktest($block_content, $block){
//     if($block['blockName'] === 'real-media-library/gallery'){
//         return var_dump($block);
//     }

//     return $block_content;
    
// }

//add_filter( 'render_block', 'rtwp_blocktest', 10, 2 );