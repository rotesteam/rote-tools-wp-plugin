<?php

// Registrieren einer Custom-Taxonomie für die Gremien/Gruppen

function rtwp_create_section_taxonomy() {
    register_taxonomy(
        'tax_rtwp_section',
        'rtwp-person',
        array(
            'label' => __('Gremium/Gruppe'),
            'rewrite' => array('slug' => 'section'),
            'hierarchical' => true
        )
    );
}
add_action('init', 'rtwp_create_section_taxonomy');


// Registrieren des Coustom-Type Person

function register_rtwp_person_cpt() {

  // Define the custom post type arguments
  $args = [
    'label' => 'Personen',
    'public' => true,
    'hierarchical' => false,
    'has_archive' => true,
    'menu_icon' => 'dashicons-groups',
    'show_in_rest' => true,
    'supports' => [ 'permalink','thumbnail','custom-fields'],
    'rewrite' => array('slug' => 'person'),
  ];

  // Register the custom post type
  register_post_type('rtwp-person', $args);

}

// Hook into the 'init' action to register the custom post type
add_action('init', 'register_rtwp_person_cpt');


//Metabox für custom-fields beim CPT "Person"

function rtwp_add_person_meta_boxes() {
    add_meta_box(
        'rtwp-person-info',
        'Angaben zur Person',
        'rtwp_person_info_meta_box_callback',
        'rtwp-person',
        'normal',
        'high'
    );
}
add_action('add_meta_boxes', 'rtwp_add_person_meta_boxes');



//Styles für die Anzeige der Datenfelder im Backend
function rtwp_admin_head() {
    ?>
    <style>
        .modal-backdrop.show{
            display:none;
        }
        .form-floating > label{
            padding-left: 1em;
            opacity: .65;
        }
        .rtwp-function-container{
            margin-top:1em;
            padding: 0.5em;
            background-color: #eee;
        }
        .postbox .inside{
            font-size:unset;
        }
        .rtwp-function-container h3{
            margin:0.3em;
        }
        .rtwp-function{
            padding: 1em;
        }
    .rtwp-remove-function {
        color: #ff0000 !important;
        border: 1px solid red !important;
        float: right;
    }
    </style>
    <?php
    }
    add_action('admin_head', 'rtwp_admin_head');


//Metabox für die Datenfelder

function rtwp_person_info_meta_box_callback($post) {


    wp_nonce_field('rtwp_save_person_info', 'rtwp_person_info_nonce');
    $first_name = get_post_meta($post->ID, '_rtwp_first_name', true);
    $last_name = get_post_meta($post->ID, '_rtwp_last_name', true);
    $nickname = get_post_meta($post->ID, '_rtwp_nickname', true);
    $pronom = get_post_meta($post->ID, '_rtwp_pronom', true);
    $mainsection = get_post_meta($post->ID, '_rtwp_mainsection', true);
    $editor_show = get_post_meta($post->ID, '_rtwp_editor_show', true);
    
    echo '<div class="rtwp-person-admin">';
    
    echo '<div class="row g-4">';

    echo '<div class="form-floating col-md-4">';
    echo '<input class="form-control form-control-lg" type="text" id="rtwp_first_name" placeholder="" name="rtwp_first_name" value="' . esc_attr($first_name) . '" />';
    echo '<label for="rtwp_first_name">Vorname</label>';
    echo '</div>';
       
    echo '<div class="form-floating col-md-4">';
    echo '<input class="form-control form-control-lg" type="text" id="rtwp_last_name" placeholder="Nachname" name="rtwp_last_name" value="' . esc_attr($last_name) . '" />';
    echo '<label for="rtwp_last_name">Nachname</label>';
    echo '</div>';
   
    echo '<div class="form-floating col-md">';
    echo '<input class="form-control" type="text" id="rtwp_nickname" placeholder="Spitzname" name="rtwp_nickname" value="' . esc_attr($nickname) . '" />';
    echo '<label for="rtwp_nickname">Spitzname</label>';
    echo '</div>';

    echo '<div class="form-floating col-md">';
    echo '<input class="form-control" type="text" id="rtwp_pronom" placeholder="Pronom" name="rtwp_pronom" value="' . esc_attr($pronom) . '" />';
    echo '<label for="rtwp_pronom">Pronom</label>';
    echo '</div>';
    
    echo '<div class="form-floating col-md-4">';
    wp_dropdown_categories(array(
        'taxonomy' => 'tax_rtwp_section',
        'hide_empty' => 0,
        'name' => 'rtwp_mainsection',
        'class' => 'form-select',
        'show_option_none' => '-- auswählen --',
        'selected' => isset($mainsection) ? intval($mainsection) : '',
    ));
    echo '<label for="rtwp_mainsection">Mitglied in</label>';
    echo '</div>';

   // Button für Modal Weitere Einstellungen

    echo '<div class="col-md float-md-end">';
    echo '<button type="button" class="btn btn-primary" data-bs-toggle="offcanvas" data-bs-target="#rtwpModalAdvance">';
    echo '  Weitere Einstellungen';
    echo '</button>';

    echo '</div>';

    echo '<div class="rtwp-function-container">';
    echo '<h3>Funktionen</h3>';

   

    // Funktion für Repeaterfeld
    
    $functions = get_post_meta($post->ID, '_rtwp_functions', true);
    if (!is_array($functions)) {
        $functions = array();
    }
    //print_r($functions);
    foreach ($functions as $index => $function) {
        echo '<div class="rtwp-function row g-4">';

        echo '<div class="form-floating col-md-4">';
        echo '<input class="form-control" type="text" id="rtwp_functions[' . $index . '][title]" name="rtwp_functions[' . $index . '][title]" value="' . esc_attr($function['title']) . '" placeholder=" " />';        
        echo '<label class="form-label" for="rtwp_functions[' . $index . '][title]">Funktionsbezeichnung</label>';
        echo '</div>';

        echo '<div class="form-floating col-md-4">';
        wp_dropdown_categories(array(
            'taxonomy' => 'tax_rtwp_section',
            'hide_empty' => 0,
            'name' => 'rtwp_functions['. $index .'][section]',
            'class' => 'form-select',
            'show_option_none' => '-- auswählen --',
            'selected' => isset($function['section']) ? intval($function['section']) : '',
        ));
        echo '<label for="rtwp_functions['. $index .'][section]">Gremium/Gruppe</label>';
        echo '</div>';
       
        echo '<div class="form-floating col-md-12">';
        echo '<textarea class="form-control" id="rtwp_functions[' . $index . '][description]" name="rtwp_functions[' . $index . '][description]" placeholder=" " style="height:5em">' . esc_textarea(isset($function['description']) ? $function['description'] : '') . '</textarea>';
        echo '<label for="rtwp_functions[' . $index . '][description]">Beschreibung</label>';
        echo '</div>';
        
        echo '<div class="col-auto">';
        echo '<button type="button" class="btn btn-outline-danger btn-sm">Funktion entfernen</button>';
        echo '</div>';

        echo '<hr>';
        echo '</div>';
        
       
    }
    echo '<button type="button" class="button rtwp-add-function"> Funktion hinzufügen </button>';
 // Vorlage zum Hinzufügen eines Neuen Field-Set für die Funktion einer Person
 // To-Do: Anzeige/Layout Anpassen das der normalen Anzeige   
    echo '<div class="rtwp-function-template" style="display:none;">';
    echo '<div class="rtwp-function">';
    echo '<label>Funktionsbezeichnung: </label>';
    echo '<input type="text" name="rtwp_functions[__index__][title]" value="" />';
    echo '<label>Gremium: </label>';
    wp_dropdown_categories(array(
        'taxonomy' => 'tax_rtwp_section',
        'hide_empty' => 0,
        'name' => 'rtwp_functions[__index__][section]',
        'show_option_none' => '-- auswählen --',
         ));
    echo '<button type="button" class="button rtwp-remove-function">Funktion entfernen</button>';
    echo '<br />';
    echo '<label>Beschreibung: </label>';
    echo '<textarea name="rtwp_functions[__index__][description]"></textarea>';
    echo '</div>'; 
    echo '</div>'; 
    echo '</div>';

  
     
    echo '</div>';

// Off-Canvas zur Anzeige der "weitere Einstellungen"
// TO-Do: richtigen Ort finden, bzw. Css anpassen damit alles immer sichtbar ist.  

echo '<div class="offcanvas offcanvas-top" tabindex="-1" id="rtwpModalAdvance" aria-labelledby="rtwpModalAdvanceLabel">';
echo '  <div class="offcanvas-header">';
echo '    <h5 class="offcanvas-title" id="rtwpModalAdvanceLabel">Einstellungen</h5>';
echo '    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>';
echo '  </div>';
echo '  <div class="offcanvas-body">';
echo '    <div class="form-check form-switch">';
echo '      <input class="form-check-input" type="checkbox" role="switch" id="rtwp_editor_show" name="rtwp_editor_show" value="1"' . checked($editor_show, 1, false) . '>';
echo '      <label class="form-check-label" for="rtwp_editor_show">Eigene Seite für Person aktivieren</label>';
echo '    </div>';
echo '    <span><small>Für die Übernahme der Änderung muss die Seite gespeichert bzw. aktualisiert werden</small></span>';
echo '    <input type="submit" accesskey="p" tabindex="5" value="Custom Save" class="btn btn-primary mt-3" id="custom-save-post" name="publish">';
echo '  </div>';
echo '</div>';


}

//Speichern der Custom-Fields

function rtwp_save_person_info($post_id) {
    if (!isset($_POST['rtwp_person_info_nonce'])) {
        return;
    }
    if (!wp_verify_nonce($_POST['rtwp_person_info_nonce'], 'rtwp_save_person_info')) {
        return;
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }
    if (!isset($_POST['rtwp_first_name']) || !isset($_POST['rtwp_last_name']) || !isset($_POST['rtwp_nickname'])) {
        return;
    }
    $first_name = sanitize_text_field($_POST['rtwp_first_name']);
    $last_name = sanitize_text_field($_POST['rtwp_last_name']);
    $nickname = sanitize_text_field($_POST['rtwp_nickname']);
    $pronom = sanitize_text_field($_POST['rtwp_pronom']);
    $mainsection = sanitize_text_field($_POST['rtwp_mainsection']);
    $editor_show = isset($_POST['rtwp_editor_show']) ? 1 : 0;
    update_post_meta($post_id, '_rtwp_editor_show', $editor_show);
    update_post_meta($post_id, '_rtwp_first_name', $first_name);
    update_post_meta($post_id, '_rtwp_last_name', $last_name);
    update_post_meta($post_id, '_rtwp_nickname', $nickname);
    update_post_meta($post_id, '_rtwp_pronom', $pronom);
    update_post_meta($post_id, '_rtwp_mainsection', $mainsection);
    if (isset($_POST['rtwp_functions'])) {
        $functions = array();
        $sections = array();
        foreach ($_POST['rtwp_functions'] as $index => $function) {
            if (!isset($function['title']) || !isset($function['section']) || !isset($function['description'])) {
                continue;
            }
            if (empty($function['title']) && ($function['section'] === '-1' || empty($function['section'])) && empty($function['description'])) {
                continue;
            }
            $title = sanitize_text_field($function['title']);
            $section = intval($function['section']);
            $sections[] = $section;
            $description = sanitize_textarea_field($function['description']);
            $functions[] = array(
                'title' => $title,
                'section' => $section,
                'description' => $description
            );
        }
        update_post_meta($post_id, '_rtwp_functions', $functions);
        wp_set_object_terms($post_id, $sections, 'tax_rtwp_section');
    } else {
        delete_post_meta($post_id, '_rtwp_functions');
        wp_delete_object_term_relationships($post_id, 'tax_rtwp_section');
    }
}
add_action('save_post', 'rtwp_save_person_info');

function rtwp_admin_enqueue_scripts() {
    wp_enqueue_script('jquery');
}
add_action('admin_enqueue_scripts', 'rtwp_admin_enqueue_scripts');

function rtwp_admin_footer() {
?>

<script>
// Script zum einfügen eines neuen Field-Set für die Funktion des Person. 
jQuery(document).ready(function($) {
    $('.rtwp-add-function').on('click', function() {
        var index = $('.rtwp-function').length;
        var template = $('.rtwp-function-template').html();
        template = template.replace(/__index__/g, index);
        $(this).before(template);
    });
    $(document).on('click', '.rtwp-remove-function', function() {
        $(this).closest('.rtwp-function').remove();
    });
});
</script>
<?php
}
add_action('admin_footer', 'rtwp_admin_footer');

// Anzeigen im Dashoboard

add_action('wp_dashboard_setup', 'rtwp_add_person_list_dashboard_widget');

function rtwp_add_person_list_dashboard_widget() {
    wp_add_dashboard_widget(
        'rtwp-person-list',
        'Personenliste',
        'rtwp_person_list_dashboard_widget_callback'
    );
}

function rtwp_person_list_dashboard_widget_callback() {
    $query = new WP_Query(array(
        'post_type' => 'rtwp-person',
        'posts_per_page' => -1
    ));
    echo '<table class="table table-sm">';
    echo '<thead>';
    echo '<tr>';
    echo '<th scope="col">Name</th>';
    echo '<th scope ="col">Gremien und Funktionen</th>';
    echo '</tr>';
    echo '</thead>';
    while ($query->have_posts()) {
        $query->the_post();
        $first_name = get_post_meta(get_the_ID(), '_rtwp_first_name', true);
        $last_name = get_post_meta(get_the_ID(), '_rtwp_last_name', true);
        $functions = get_post_meta(get_the_ID(), '_rtwp_functions', true);
        if (!is_array($functions)) {
            $functions = array();
        }
        echo '<tr>';
        echo '<td><a href="' . esc_url(get_edit_post_link()) . '">' . esc_html($first_name . ' ' . $last_name) . '</a></td>';
           echo '<td>';
        foreach ($functions as $function) {
            $section = get_term($function['section'], 'tax_rtwp_section');
            if ($section && !is_wp_error($section)) {
                echo esc_html($section->name) . ': ';
            }
            echo esc_html($function['title']) . '<br />';
        }
        echo '</td>';
        echo '</tr>';
    }
    wp_reset_postdata();
    echo '</table>';
}


// Admintabelle um Gremien und Funktion ergänzen

add_filter('manage_rtwp-person_posts_columns', 'rtwp_add_person_list_table_columns');
add_action('manage_rtwp-person_posts_custom_column', 'rtwp_person_list_table_custom_column', 10, 2);

function rtwp_add_person_list_table_columns($columns) {
    unset($columns['date']);
    $columns['rtwp_sections_and_functions'] = 'Gremien und Funktionen';
    return $columns;
}

function rtwp_person_list_table_custom_column($column, $post_id) {
    if ($column === 'rtwp_sections_and_functions') {
        $functions = get_post_meta($post_id, '_rtwp_functions', true);
        if (!is_array($functions)) {
            $functions = array();
        }
        foreach ($functions as $function) {
            $section = get_term($function['section'], 'tax_rtwp_section');
            if ($section && !is_wp_error($section)) {
                echo esc_html($section->name) . ': ';
            }
            echo esc_html($function['title']) . '<br />';
        }
    }
}

add_filter('manage_edit-rtwp-person_sortable_columns', 'rtwp_person_list_table_sortable_columns');
add_action('pre_get_posts', 'rtwp_person_list_table_orderby');

function rtwp_person_list_table_sortable_columns($columns) {
    $columns['rtwp_sections_and_functions'] = 'rtwp_sections_and_functions';
    return $columns;
}

function rtwp_person_list_table_orderby($query) {
    if (!is_admin() || !$query->is_main_query()) {
        return;
    }
    if ($query->get('orderby') === 'rtwp_sections_and_functions') {
        $query->set('meta_key', '_rtwp_functions');
        $query->set('orderby', 'meta_value');
    }
}


// Ändert den Namen des Beitrag beim abspeichern

add_action( 'save_post', 'rtwp_save_person_title', 10, 3 );
function rtwp_save_person_title( $post_id, $post, $update ) {
    // Überprüfen, ob es sich um den richtigen Beitragstyp handelt
    if ( $post->post_type != 'rtwp-person' ) {
        return;
    }
    // // Überprüfen, ob der Titel bereits gesetzt ist
    // if ( ! empty( $post->post_title ) ) {
    //     return;
    // }
    // Vor- und Nachnamen abrufen
    $first_name = get_post_meta( $post_id, '_rtwp_first_name', true );
    $last_name = get_post_meta( $post_id, '_rtwp_last_name', true );
    // Titel aktualisieren
    $new_title = trim( $first_name . ' ' . $last_name );
    if ( ! empty( $new_title ) ) {
        // Verhindern, dass die save_post-Aktion erneut ausgelöst wird
        remove_action( 'save_post', 'rtwp_save_person_title' );
        // Titel aktualisieren
        wp_update_post( array(
            'ID' => $post_id,
            'post_title' => $new_title,
            'post_name' => sanitize_title( $new_title )
        ) );
        // Aktion erneut hinzufügen
        add_action( 'save_post', 'rtwp_save_person_title', 10, 3 );
    }
}




function register_blocks_person() {
    register_block_type( plugin_dir_url(__FILE__) . 'dist/blocks/person_accordion/block.json' );
}
//add_action( 'init', 'register_blocks_person' );

function my_enqueue_block_editor_assets() {
    wp_enqueue_script(
        'my-block',
        plugin_dir_url(__FILE__) . 'dist/blocks/my_block/my_block.js',
        array( 'wp-blocks', 'wp-element', 'wp-components', 'wp-data' )
    );
}
//add_action( 'enqueue_block_editor_assets', 'my_enqueue_block_editor_assets' );

// Editor nur anzeigen wenn der bei der Person selbst aktiviert ist. 

add_filter( 'register_post_type_args', 'rtwp_dynamic_editor_support', 10, 2 );
function rtwp_dynamic_editor_support( $args, $post_type ) {
    // Überprüfen, ob es sich um den richtigen Beitragstyp handelt
    if ( $post_type != 'rtwp-person' ) {
        return $args;
    }
    // Überprüfen, ob wir uns auf der Bearbeitungsseite befinden
    if ( ! is_admin() || ! isset( $_GET['post'] ) ) {
        return $args;
    }
    // Wert des benutzerdefinierten Felds abrufen
    $post_editor =$_GET['post'];
    $show_editor = get_post_meta($post_editor, '_rtwp_editor_show', true);
    // Editor basierend auf dem Wert des benutzerdefinierten Felds anzeigen/ausblenden
    if ( $show_editor == 1 ) {
        // Editor anzeigen
        if ( ! in_array( 'editor', $args['supports'] ) ) {
            $args['supports'][] = 'editor';
        }
    } else {
        // Editor ausblenden
        if ( ( $key = array_search( 'editor', $args['supports'] ) ) !== false ) {
            unset( $args['supports'][ $key ] );
        }
    }
    return $args;
}
