<?php
/* Exit if file access directly */
if ( ! defined( 'ABSPATH' ) ) exit;

//include_once('rtwp_opengraph_acf.php');

// acf_add_options_sub_page(array(
//     'page_title' 	=> 'Einstellungen für Opengraph',
//     'menu_title'	=> __('Opengraph'),
//     'menu_slug'		=> 'rtwp-opengraph',
//     'capability'	=> 'edit_private_pages',
//     'parent_slug' 	=> $rtwp_settings_optionpage['menu_slug'],
//     'update_button'	=> 'Übernehmen',
//     'post_id'		=> 'rtwp-opengraph',
// )); 

// function rtwp_opengraph ($og_type='website') {
//     global $post;

//     $og_title;
//     $og_url;
//     $og_desc;
//     $og_sitename;

// };


// add_action( 'add_meta_boxes', function() {
//     add_meta_box(
//         'open_graph_meta_editor',
//         __( 'Open Graph Optimierung' ),
//         'rtwp_open_graph',
//         'page',
//         'side'
//     );
// });



function rtwp_open_graph( $post ) {
    // Get the current Open Graph metadata.
    $open_graph = get_post_meta( $post->ID, 'rtwp_opengraph', true );
    
    // Render the metabox form.
    var_dump($open_graph);
    ?>
    <div class="form-group">
        <label for="og-title">Title</label>
        <input type="text" name="og-title" id="og-title" value="<?php echo esc_attr( $open_graph['og:title'] ); ?>" />
    </div>
    <div class="form-group">
        <label for="og-type"><?php _e( 'Type', 'rtwp' ); ?></label>
        <select name="og-type" id="og-type">
            <option value="">Select Type</option>
            <option value="article">Article</option>
            <option value="book">Book</option>
            <option value="image">Image</option>
            <option value="music">Music</option>
            <option value="video">Video</option>
            <option value="website">Website</option>
        </select>
    </div>
    <div class="form-group">
        <label for="og-image">Image</label>
        <input type="url" name="og-image" id="og-image" value="<?php echo esc_attr( $open_graph['og:image'] ); ?>" />
    </div>
    <div class="form-group">
        <label for="og-description">Description</label>
        <textarea name="og-description" id="og-description"><?php echo esc_textarea( $open_graph['og:description'] ); ?></textarea>
    </div>
    <?php
}

add_action( 'save_post', function( $post_id ) {
    // Only save the metabox data if the post is being saved.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Get the metabox data.
    $data = array(
        'og:title' => filter_input( INPUT_POST, 'og-title', FILTER_SANITIZE_STRING ),
        'og:type' => filter_input( INPUT_POST, 'og-type', FILTER_SANITIZE_STRING ),
        'og:image' => filter_input( INPUT_POST, 'og-image', FILTER_SANITIZE_URL ),
        'og:description' => filter_input( INPUT_POST, 'og-description', FILTER_SANITIZE_STRING ),
    );

    // Update the post meta.
    update_post_meta( $post_id, 'rtwp_opengraph', $data );
});






 /**
 * Render the options pages
 */

    // Main Page
    function rtwp_render_settings_page(){
        echo '<h1>RoteTools Einstellungen</h1>';
    }

    //Submenu OpenGraph
    function rtwp_render_option_page() {
     ?>
    <div class="wrap">
        <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
 
        <form action="options.php" method="post">
            <?php
            // Use nonce for verification
            wp_nonce_field( plugin_basename(__FILE__), 'rtwp_option_page_noncename' );
             
            settings_fields('rtwp_opengraph_settings_section');
            do_settings_sections('rtwp-settings/rtwp-og-settings');
            submit_button();
            ?>
        </form>
    </div>
    <?php
    }

 /**
  * Felder für die Setting-Seite zur Einstellung der OpenGraph vorgaben.
  */

 add_action('admin_init', 'rtwp_opengraph_setting_section');  

 function rtwp_opengraph_setting_section() {  
    add_settings_section(  
        'rtwp_opengraph_settings_section', // Section ID 
        'Grundeinstellungen', // Section Title
        'rtwp_opengraph_setting_section_callback', // Callback
        'rtwp-settings/rtwp-og-settings' // What Page?  This makes the section show up on the General Settings Page
    );
    
    add_settings_field( // Option 1
        'title', // Option ID
        'Titel', // Label
        'rtwp_fields_textbox_callback', // !important - This is where the args go!
        'rtwp-settings/rtwp-og-settings', // Page it will be displayed (General Settings)
        'rtwp_opengraph_settings_section', // Name of our section
        array( // The $args
            'title' // Should match Option ID
        )  
    ); 
    
    add_settings_field( // Option 2
        'description', // Option ID
        'Beschreibung', // Label
        'rtwp_fields_textarea_callback', // !important - This is where the args go!
        'rtwp-settings/rtwp-og-settings', // Page it will be displayed
        'rtwp_opengraph_settings_section', // Name of our section (General Settings)
        array( // The $args
            'description' // Should match Option ID
        )  
    ); 
    
    register_setting('rtwp_opengraph_settings_section','title', 'esc_attr');
    register_setting('rtwp_opengraph_settings_section','description', 'esc_html');
}

function rtwp_opengraph_setting_section_callback() { // Section Callback
    echo '<p>Open Graph wird hauptsächlich genutzt, um das Teilen von Seiten einer Website zu optimieren. Anstatt ein automatisch generiertes Bild mit Beschreibung anzuzeigen, kann mittels Open Graph Metadaten der Social-Media-Plattform genau mitgeteilt werden, wie eine geteilte Webseite dargestellt werden soll. </p>
    <p>Hier können die Standardangaben für die OpenGraph Angaben angegeben werden.<br>
    Auch auf einzelenen Seiten können individuelle Angaben gemacht werden. Sind dort keine Angaben eingetragen, werden diese hier übernommen.</p>';  
}


// option-fields funtions

function rtwp_fields_textbox_callback($args) {  // Textfield Callback
    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
}

function rtwp_fields_textarea_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
    echo '<textarea id="'. $args[0] .'" name="'. $args[0] .'">'. $option .'</textarea>';
}
 
 

/**
 * Register custom fields for OpenGraph information in the posttype page in WordPress.
 */

add_action( 'add_meta_boxes', 'add_opengraph_meta_box' );

function add_opengraph_meta_box() {
    add_meta_box(
        'opengraph_meta_box',
        __( 'OpenGraph Information', 'your-text-domain' ),
        'opengraph_meta_box_content',
        'page',
        'side',
        'high'
    );
}

function get_character_count($string) {
    return strlen(strip_tags($string));
}

function get_word_count($string) {
    return str_word_count(strip_tags($string));
}



function opengraph_meta_box_content() {
    // Get the post ID.
    $post_id = get_the_ID();

    // Get the current Open Graph metadata.
    $open_graph = get_post_meta( $post_id, 'rtwp_opengraph', true );


    // Display the metabox content.
    ?>
    <style>
    .form-group{padding-top:0.6em;}
    .form-group input[type="text"], .form-group textarea {
        border-width:5px;
    }</style>
    <div><?php _e( 'Hier können die OpenGraph Angaben angepasst werden', 'rtwp' ); ?></div>
   
    <div class="form-group" id="rtwp-og-counter-title">
        <label for="og_title"><strong><?php _e( 'Titel', 'rtwp' ); ?></strong></label><br>
        <input type="text" name="og_title" id="og_title" placeholder ="Hier einen individuellen Titel angeben" value="<?php echo esc_attr( $open_graph['og:title'] ); ?>" />
    </div>

    <div class="form-group" id="rtwp-og-counter-desc">
        <label for="og_description"><strong><?php _e( 'Beschreibung', 'rtwp' ); ?></strong></label><br>
        <textarea name="og_description" id="og_description"><?php echo esc_textarea( $open_graph['og:description'] ); ?></textarea>
    </div>
        

    <div class="form-group">
        <br>
    <?php if( $image = wp_get_attachment_image_url( $open_graph['og:image'], 'medium' ) ): ?>
	<a href="#" class="rtwp-upload">
		<img src="<?php echo esc_url( $image ) ?>" />
	</a>
	<a href="#" class="rtwp-remove">Vorschaubild entfernen</a>
	<input type="hidden" name="og_image" value="<?php echo absint( $open_graph['og:image']) ?>">
<?php else : ?>
	<a href="#" class="button rtwp-upload">Vorschaubild wählen</a>
	<a href="#" class="rtwp-remove" style="display:none">Vorschaubild entfernen</a>
	<input type="hidden" name="og_image" value="">
<?php endif; ?>
    </div>

    <div class="form-group">
        <label for="og_type"><strong><?php _e( 'Type', 'rtwp' ); ?></strong></label><br>
        <select name="og_type" id="og_type">
            <option value="">Select Type</option>
            <option value="article">Article</option>
            <option value="book">Book</option>
            <option value="image">Image</option>
            <option value="music">Music</option>
            <option value="video">Video</option>
            <option value="website">Website</option>
        </select>
    </div>
    

<!-- Buttons zum zuschalten der weiteren Einstellungen -->
<div>
<button id="twitter-active-button">
    <i class="fa fa-twitter fa-2x"></i>
</button>

<button id="advance-button">
    <i class="fa fa-cog fa-2x"></i>
</button>
</div>
<!-- Die Feldgruppe, die zunächst versteckt ist -->
<div class="form-group" id="rtwp-hidden-twitter" style="display: none;">
    <div class="dorm-group" id="rtwp-twitter-counter-title" >
        <label for="twitter_title"><strong><?php _e( 'Twitter Titel', 'rtwp' ); ?></strong></label>
        <input type="text" name="twitter_title" id="twitter_title" placeholder ="Hier einen individuellen Titel angeben" value="<?php echo esc_attr( $open_graph['twitter:title'] ); ?>" />
    </div>
    <div class="form-group" id="rtwp-twitter-counter-desc">
        <label for="twitter_description"><?php _e( 'Beschreibung für Twitter', 'rtwp' ); ?></label>
        <textarea name="twitter_description" id="twitter_description"><?php echo esc_textarea( $open_graph['twitter:description'] ); ?></textarea>
    </div>

    <div class="form-group">
        <label for="twitter_type"><?php _e( 'Cardtyp', 'rtwp' ); ?></label>
        <select name="twitter_type" id="twitter_type">
            <option value="summary">Klein</option>
            <option value="summary_large_image">Groß</option>
        </select>
    </div>
</div>

<div class="form-group" id="rtwp-hidden-advance" style="display: none;">
    <div class="form-group" >
        <label for="og_url"><strong><?php _e( 'URL', 'rtwp' ); ?></strong></label>
        <input type="text" name="og_url" id="og_url" placeholder ="Hier eine URL zum Beitrag auf den umgelenkt wird beim teilen" value="<?php echo esc_attr( $open_graph['og:url'] ); ?>" />
    </div>
    <div class="form-group" id="rtwp-twitter-counter-desc">
        <label for="twitter_description"><?php _e( 'Beschreibung für Twitter', 'rtwp' ); ?></label>
        <textarea name="twitter_description" id="twitter_description"><?php echo esc_textarea( $open_graph['twitter:description'] ); ?></textarea>
    </div>

    <div class="form-group">
        <label for="twitter_type"><?php _e( 'Cardtyp', 'rtwp' ); ?></label>
        <select name="twitter_type" id="twitter_type">
            <option value="summary">Klein</option>
            <option value="summary_large_image">Groß</option>
        </select>
    </div>
</div>

<script>
    // Zeige die Feldgruppe an, wenn auf den Twitter-Button geklickt wird
    document.querySelector( '#twitter-active-button' ).addEventListener( 'click', function() {
        var fields = document.querySelector( '#rtwp-hidden-twitter' );
        if ( fields.style.display === 'none' ) {
            fields.style.display = 'block';
        } else {
            fields.style.display = 'none';
        }
    } );
    // Zeige die Feldgruppe an, wenn eines der Felder einen Wert hat
    window.addEventListener( 'load', function() {
        var input1 = document.querySelector( '#twitter_title' );
        var input2 = document.querySelector( '#twitter_description' );
        if ( input1.value || input2.value ) {
            document.querySelector( '#rtwp-hidden-twitter' ).style.display = 'block';
        } else {
            document.querySelector( '#rtwp-hidden-twitter' ).style.display = 'none';
        }
    } );
</script>

    



<script>




jQuery(document).ready(function($) {

    var ogTitle =$('#og_title');
    var ogDesc =$('#og_description');
    var ogTitleOptLength = 60;
    var ogTitleMaxLength = 95;
    var ogDescMaxLength = 300;
    var ogDescOptLength = 200;
    

    function ogColor(length, optLength, maxLength) {
        if (length > maxLength) {
        return 'red';
        } else if (length > optLength ) {
        return 'orange';
        } else {
        return 'green';
        }
    }

    function ogLength(field) {
        return field.val().length;
    }

    // Titel-Zeichen-Zählen

    $('#rtwp-og-counter-title').append('<span class="rtwp-og-title-count">' + ogLength(ogTitle) + '/' + ogTitleMaxLength + '</span> <span><small>Optimal sind Titel bis max. 50 Zeichen, danach wird umgebrochen oder gekürzt.</small></span>');
    $('.rtwp-og-title-count').css('color', ogColor(ogLength(ogTitle), ogTitleOptLength, ogTitleMaxLength));

    ogTitle.on('input', function(){
        var newLength = $(this).val().length;
        $('.rtwp-og-title-count').text(newLength + '/' + ogTitleMaxLength);
        $(this).css('border-color', ogColor(ogLength(ogTitle), ogTitleOptLength, ogTitleMaxLength));
        $('.rtwp-og-title-count').css('color', ogColor(ogLength(ogTitle), ogTitleOptLength, ogTitleMaxLength));    
    });

    // Beschreibungs-Zeichen-Zählen

    $('#rtwp-og-counter-desc').append('<span class="rtwp-og-desc-count">' + ogLength(ogDesc) + '/' + ogDescMaxLength + ' </span> <span>  <small>  Die optimale Länge ist bis zu 200 Zeichen. Facebook kann auch 300.</small></span>');
    $('.rtwp-og-desc-count').css('color', ogColor(ogLength(ogDesc), ogDescOptLength, ogDescMaxLength));
    
    ogDesc.on('input', function(){
        var newLength = $(this).val().length;
        $('.rtwp-og-desc-count').text(newLength + '/' + ogDescMaxLength);
        $(this).css('border-color', ogColor(ogLength(ogDesc), ogDescOptLength, ogDescMaxLength));
        $('.rtwp-og-desc-count').css('color', ogColor(ogLength(ogDesc), ogDescOptLength, ogDescMaxLength));
    });

    // Titel-Zeichen-Twitter-Zählen

    var twitterTitle =$('#twitter_title');
    var twitterDesc =$('#twitter_description');

    $('#rtwp-twitter-counter-title').append('<span class="rtwp-twitter-title-count">' + ogLength(twitterTitle) + '/' + ogTitleMaxLength + '</span> <span><small>Optimal sind Titel bis max. 50 Zeichen, danach wird umgebrochen oder gekürzt.</small></span>');
    $('.rtwp-twitter-title-count').css('color', ogColor(ogLength(twitterTitle), ogTitleOptLength, ogTitleMaxLength));

    twitterTitle.on('input', function(){
        var newLength = $(this).val().length;
        $('.rtwp-twitter-title-count').text(newLength + '/' + ogTitleMaxLength);
        $(this).css('border-color', ogColor(ogLength(twitterTitle), ogTitleOptLength, ogTitleMaxLength));
        $('.rtwp-twitter-title-count').css('color', ogColor(ogLength(twitterTitle), ogTitleOptLength, ogTitleMaxLength));    
    });

    $('#rtwp-twitter-counter-desc').append('<span class="rtwp-twitter-desc-count">' + ogLength(twitterDesc) + '/' + ogDescMaxLength + ' </span> <span>  <small>  Die maximale Länge für Twitter liegt bei 200 Zeichen.</small></span>');
    $('.rtwp-twitter-desc-count').css('color', ogColor(ogLength(ogDesc), ogDescOptLength, ogDescMaxLength));
    

    twitterDesc.on('input', function(){
        var newLength = $(this).val().length;
        $('.rtwp-twitter-desc-count').text(newLength + '/' + ogDescMaxLength);
        $(this).addClass('border border-5');
        $(this).css('border-color', ogColor(ogLength(twitterDesc), ogDescOptLength, ogDescMaxLength));
        $('.rtwp-twitter-desc-count').css('color', ogColor(ogLength(twitterDesc), ogDescOptLength, ogDescMaxLength));
    });
    
});

</script>
    <?php
    
}

add_action( 'save_post', 'save_opengraph_meta_box' );


function save_opengraph_meta_box( $post_id ) {
    // Check if this is an auto save routine.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check if this is an approved post.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return;
    }

    // Get the post data.
    $post_data = $_POST;

     // Get the metabox data.
     $data = array(
        'og:title' => filter_input( INPUT_POST, 'og_title', FILTER_SANITIZE_STRING ),
        'og:type' => filter_input( INPUT_POST, 'og_type', FILTER_SANITIZE_STRING ),
        'og:image' => filter_input( INPUT_POST, 'og_image', FILTER_SANITIZE_URL ),
        'og:description' => filter_input( INPUT_POST, 'og_description', FILTER_SANITIZE_STRING ),
        'twitter:title' => filter_input(INPUT_POST,'twitter_title', FILTER_SANITIZE_STRING),
        'twitter:description' => filter_input(INPUT_POST,'twitter_description', FILTER_SANITIZE_STRING),
    );

    // Update the post meta.
    update_post_meta( $post_id, 'rtwp_opengraph', $data );
}

/**
 * Funktion um die OpenGraph Daten in den Header des Template hinzuzufügen. 
 */

 function rtwp_opengraph_theme_header(){
    global $post;

    $open_graph = get_post_meta( $post->ID, 'rtwp_opengraph', true );
    if($open_graph){
    $og_title = get_the_title();
    if ($open_graph['og:title']){
        $og_title = $open_graph['og:title'];
    }
    $out = '<meta property="og:title" content="' . $og_title . '"/>';
    //$out .= '<meta property="og:url" content=""/>';

    if ($open_graph['og:type']){
        $og_type = $open_graph['og:type'];
        $out .= '<meta property="og:type" content="' . $og_type . '"/>';
    }

    if ($open_graph['og:image']){
        $og_image = wp_get_attachment_image_url( $open_graph['og:image'], 'medium' );
        $out .= '<meta property="og:image" content="' . $og_image . '"/>';
    }

    if ($open_graph['og:description']){
        $og_description = $open_graph['og:description'];
        $out .= '<meta property="og:description" content="' . $og_description . '"/>';
    }

    if ($open_graph['twitter:title']){
        $out .= '<meta name="twitter:card" content="summary_large_image"/>';
        $out .= '<meta name="twitter:title" content="'. $open_graph['twitter:title'] . '"/>';
    }

    if ($open_graph['twitter:description']){
        $twitter_description = $open_graph['twitter:description'];
        $out .= '<meta name="twitter:description" content="' . $twitter_description . '"/>';
    }

    $out .= '<meta />';
    echo $out;
    }
 }

 add_action('wp_head','rtwp_opengraph_theme_header');


 
?>



